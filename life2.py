class Field:
    pos = {}
    next_gen = {}

    def __init__(self):
        self.pos = {}
        self.next_gen = {}

    def get(self, x=None, y=None):
        if x is None and y is None:
            return self.pos

        elif x is not None and y is None:
            if x in self.pos.keys():
                return self.pos[x]
        else:
            if x in self.pos.keys():
                if y in self.pos[x]:
                    return True
                else:
                    return False
            else:
                return False

    def set(self, x, y):
        if x in self.pos.keys():
            self.pos[x].append(y)
        else:
            self.pos[x] = [y]

    def remove(self, x=None, y=None):
        if x is not None and y is not None:
            if x in self.pos.keys() and y in self.pos[x]:
                self.pos[x].remove(y)
        elif x is not None and y is None:
            if x in self.pos.keys():
                del self.pos[x]
        else:
            self.pos = {}

    def sort(self):
        for i in self.pos.keys():
            self.pos[i].sort()

    def count_neighbors_alive(self, neighbors):
        value = 0

        for x in neighbors:
            for y in neighbors[x]:
                if x in self.pos.keys() and y in self.pos[x]:
                    value += 1

        return value

    def next_generation(self, x, y, neighbors_alive):

        isset = False
        value = False

        if x in self.pos.keys() and y in self.pos[x]:
            isset = True

        if isset:
            if neighbors_alive >= 2 and neighbors_alive <= 3:
                if x in self.next_gen.keys() and y not in self.next_gen[x]:
                    self.next_gen[x].append(y)
                if x not in self.next_gen.keys():
                    self.next_gen[x] = [y]
                value = True
        else:
            if neighbors_alive == 3:
                if x in self.next_gen.keys() and y not in self.next_gen[x]:
                    self.next_gen[x].append(y)
                if x not in self.next_gen.keys():
                    self.next_gen[x] = [y]
                value = True

        return value

    def get_next_generation(self, x_max, y_max):

        for x in range(x_max):
            for y in range(y_max):
                neighbors = self.get_neighbors(x, y)
                neighbors_alive = self.count_neighbors_alive(neighbors)
                self.next_generation(x, y, neighbors_alive)
        self.pos = self.next_gen
        self.next_gen = {}


class EndlessField(Field):

    def __init__(self):
        super().__init__()

    def to_fit_positions(self):
        spacing_x = 0
        spacing_y = 0
        clipboard = {}

        for x in self.pos.keys():
            if x < spacing_x:
                spacing_x = x
            for y in self.pos[x]:
                if y < spacing_y:
                    spacing_y = y
        print(spacing_x, spacing_y)

        spacing_x = spacing_x*(-1) + 1
        spacing_y = spacing_y*(-1) + 1

        print(spacing_x, spacing_y)

        for set_x in self.pos.keys():
            for set_y in self.pos[set_x]:
                if (set_x + spacing_x) in clipboard:
                    clipboard[set_x + spacing_x].append(set_y + spacing_y)
                else:
                    clipboard[set_x + spacing_x] = [set_y + spacing_y]
        self.pos = clipboard

    def get_neighbors(self, x, y):

        neighbors = {}

        for width in range(-1, 2):
            for height in range(-1, 2):
                if not(width == 0 and height == 0):
                    if (x + width) in neighbors.keys():
                        neighbors[x + width].append(y + height)
                    else:
                        neighbors[x + width] = [y + height]
        return neighbors

    def get_next_generation(self):

        x_max, y_max = 0, 0

        for x in self.pos.keys():
            if x > x_max:
                x_max = x
            for y in self.pos[x]:
                if y > y_max:
                    y_max = y

        x_max += 2  # +1 for active neighbors and +1 to fit the range
        y_max += 2

        super().get_next_generation(x_min, y_min, x_max, y_max)


class WrappingField(Field):
    def __init__(self, x, y):
        super().__init__()
        self.x = x
        self.y = y

    def set(self, x, y):
        x = x % self.x
        y = y % self.y

        super().set(x, y)

    def get_neighbors(self, x, y):

        x %= self.x
        y %= self.y
        neighbors = {}

        for width in range(-1, 2):
            for height in range(-1, 2):
                if not (width == 0 and height == 0):
                    x_element = ((x + width) % self.x)
                    y_element = ((y + height) % self.y)

                    print(x_element, y_element)

                    if x_element in neighbors:
                        neighbors[x_element].append(y_element)
                    else:
                        neighbors[x_element] = [y_element]
        return neighbors

    def get_next_generation(self):
        super().get_next_generation(self.x, self.y)


class DeadZoneField(Field):

    def __init__(self, x, y):
        super().__init__()
        self.x = x
        self.y = y

    def set(self, x, y):
        if (x >= 0 and x < self.x) and (y >= 0 and y < self.y):
            super().set(x, y)

    def get_neighbors(self, x, y):

        neighbors = {}

        for width in range(-1, 2):
            for height in range(-1, 2):
                if not(width == 0 and height == 0):
                    if (
                        (x + width >= 0 and x + width < self.x)
                        and (y + height >= 0 and y + height < self.y)
                    ):
                        if x + width in neighbors.keys():
                            neighbors[x + width].append(y + height)
                        else:
                            neighbors[x + width] = [y + height]

        return neighbors

    def get_next_generation(self):
        super().get_next_generation(self.x, self.y)
