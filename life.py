class NormalField:
    def __init__(self, size_x: int, size_y: int):

        self.w = size_x
        self.h = size_y
        self.true_positions = []
        self.array = [[False for x in range(size_x)] for y in range(size_y)]
        self.next_gen = [[False for x in range(size_x)] for y in range(size_y)]

    def get(self, x: int, y: int):

        return self.array[x][y]

    def set(self, x: int, y: int, value: bool):

        self.array[x][y] = value

    def get_neighbors(self, x: int, y: int) -> [int]:
        position_x_y = []

        for height in range(-1, 2):
            for width in range(-1, 2):
                if not(width == 0 and height == 0):
                    if (
                        (x + width >= 0 and x + width < self.w)
                        and (y + height >= 0 and y + height < self.h)
                        ):

                        position_x_y.append(
                            [(x + width), (y + height)]
                        )

        print(position_x_y)
        return position_x_y

    def count_neighbors_alive(self, position_x_y: [int]) -> int:

        value = 0
        for i in range(len(position_x_y)):

            x = position_x_y[i][0]
            y = position_x_y[i][1]

            if self.array[x][y]:
                value += 1

        return value

    def next_generatoin(self, x: int, y: int, neighbors_alive: int):

        if self.array[x][y]:
            if neighbors_alive >= 2 and neighbors_alive <= 3:
                self.next_gen[x][y] = True
                self.true_positions.append([x, y])
            else:
                self.next_gen[x][y] = False
        else:
            if neighbors_alive == 3:
                self.next_gen[x][y] = True
                self.true_positions.append([x, y])
            else:
                self.next_gen[x][y] = False

        return self.next_gen[x][y]

    def get_next_generation(self):
        self.true_positions = []

        for width in range(self.w):
            for height in range(self.h):
                neighbors = self.get_neighbors(width, height)
                neighbors_alive = self.count_neighbors_alive(neighbors)
                self.next_generatoin(width, height, neighbors_alive)

        self.array = self.next_gen
        self.next_gen = [[False for x in range(self.w)] for y in range(self.h)]


class WrappingField:

    def __init__(self, size_x: int, size_y: int):

        self.w = size_x
        self.h = size_y
        self.true_positions = []
        self.array = [[False for x in range(size_x)] for y in range(size_y)]
        self.next_gen = [[False for x in range(size_x)] for y in range(size_y)]

    def get(self, x: int, y: int):

        x = x % (len(self.array))
        y = y % (len(self.array))

        return self.array[x][y]

    def set(self, x: int, y: int, value: bool):

        x = x % (len(self.array))
        y = y % (len(self.array))

        for height in range(len(self.array)):
            for width in range(len(self.array)):
                    self.array[width][height] = False
            width = 0

        self.true_positions.append([x, y])
        # print(len(self.true_positions))
        for element in range(len(self.true_positions)):

            x_element = self.true_positions[element][0]
            y_element = self.true_positions[element][1]

            self.array[x_element][y_element] = value
            # print(self.array[self.true_positions[element][0]][self.true_positions[element][1]])

    def get_neighbors(self, x: int, y: int) -> [int]:

        x = x % len(self.array)
        y = y % len(self.array)
        position_x_y = []

        for height in range(-1, 2):
            for width in range(-1, 2):
                if not(width == 0 and height == 0):

                    x_element = ((x + width) % len(self.array))
                    y_element = ((y + height) % len(self.array))
                    position_x_y.append(
                        [x_element, y_element]
                    )
        return position_x_y

    def count_neighbors_alive(self, position_x_y: [int]) -> int:

        value = 0
        for i in range(len(position_x_y)):

            x = position_x_y[i][0]
            y = position_x_y[i][1]

            if self.array[x][y]:
                value += 1

        return value

    def next_generatoin(self, x: int, y: int, neighbors_alive: int):

        if self.array[x][y]:
            if neighbors_alive >= 2 and neighbors_alive <= 3:
                self.next_gen[x][y] = True
                self.true_positions.append([x, y])
            else:
                self.next_gen[x][y] = False
        else:
            if neighbors_alive == 3:
                self.next_gen[x][y] = True
                self.true_positions.append([x, y])
            else:
                self.next_gen[x][y] = False

        return self.next_gen[x][y]

    def get_next_generation(self):
        self.true_positions = []

        for width in range(self.w):
            for height in range(self.h):
                neighbors = self.get_neighbors(width, height)
                neighbors_alive = self.count_neighbors_alive(neighbors)
                self.next_generatoin(width, height, neighbors_alive)

        self.array = self.next_gen
        self.next_gen = [[False for x in range(self.w)] for y in range(self.h)]


class EndlessField:
    def __init__(self):
        self.positions = []
        self.next_gen = []

    def get(self, x: int, y: int):

        value = False

        if [x, y] in self.positions:
            value = True

        return value

    def set(self, x: int, y: int, value: bool):

        if value:
            if not [x, y] in self.positions:
                self.positions.append([x, y])
        else:
            if [x, y] in self.positions:
                self.positions.remove([x, y])

        self.positions.sort()

    def to_fit_positions(self):

        spacing_x = 0
        spacing_y = 0

        for i in range(len(self.positions)):
            if self.positions[i][0] <= 0:
                spacing_x = self.positions[i][0]*(-1) + 1
            if self.positions[i][1] <= 0:
                spacing_y = self.positions[i][1]*(-1) + 1
            if spacing_x != 0 or spacing_y != 0:
                for count in range(len(self.positions)):
                    x = self.positions[count][0] + spacing_x
                    y = self.positions[count][1] + spacing_y
                    self.positions.append([x, y])
            spacing_x = 0
            spacing_y = 0

    def get_neighbors(self, x: int, y: int) -> [int]:

        position_x_y = []

        for height in range(-1, 2):
            for width in range(-1, 2):
                if not(width == 0 and height == 0):
                    position_x_y.append(
                        [x + width, y + height]
                    )

        return position_x_y

    def count_neighbors_alive(self, position_x_y: [int]):

        value = 0
        for i in range(len(position_x_y)):

            x = position_x_y[i][0]
            y = position_x_y[i][1]

            if [x, y] in self.positions:
                value += 1

        return value

    def next_generation(self, x: int, y: int, neighbors_alive: int):
        value = False
        index = None

        for i in range(len(self.positions)):
            if [x, y] == self.positions[i]:
                index = self.positions[i]

        if index:
            if neighbors_alive >= 2 and neighbors_alive <= 3:
                self.next_gen.append(index)
                value = True
        else:
            if neighbors_alive == 3:
                self.next_gen.append([x, y])
                value = True

        return value

    def get_next_generation(self):
        x_max, y_max = 0, 0
        for i in range(len(self.positions)):
            if self.positions[i][0] > x_max:
                x_max = self.positions[i][0]
            if self.positions[i][1] > y_max:
                y_max = self.positions[i][1]

        x_max += 1
        y_max += 1

        for x in range(x_max + 1):
            for y in range(y_max + 1):
                neighbors = self.get_neighbors(x, y)
                neighbors_alive = self.count_neighbors_alive(neighbors)
                self.next_generation(x, y, neighbors_alive)
        print(self.positions)
        print(self.next_gen)
        self.positions = self.next_gen
        self.next_gen = []
