def get_neighbors(x, y):
    neighbors = {
        x-1: [y-1, y, y+1],
        x: [y-1, y+1],
        x+1: [y-1, y, y+1]
    }
    return neighbors


class Field:
    pos = {}
    next_gen = {}

    def __init__(self):
        self.pos = {}
        self.next_gen = {}

    def get(self, x, y):
        if x in self.pos.keys() and y in self.pos[x]:
            return True
        else:
            return False

    def set(self, x, y, value):
        if value:
            if x in self.pos.keys() and y not in self.pos[x]:
                self.pos[x].append(y)
            if x not in self.pos.keys():
                self.pos[x] = [y]
        else:
            if self.get(x, y):
                self.pos[x].remove(y)

    def count_neighbors_alive(self, neighbors):
        counter = 0

        for width in neighbors.keys():
            for height in neighbors[width]:
                if self.get(width, height):
                    counter += 1

        return counter

    def next_generation(self, x, y, neighbors_alive):
        if self.get(x, y):
            if neighbors_alive >= 2 and neighbors_alive <= 3:
                if x in self.next_gen.keys() and y not in self.next_gen[x]:
                    self.next_gen[x].append(y)
                if x not in self.next_gen.keys():
                    self.next_gen[x] = [y]
        else:
            if neighbors_alive == 3:
                if x in self.next_gen.keys() and y not in self.next_gen[x]:
                    self.next_gen[x].append(y)
                if x not in self.next_gen.keys():
                    self.next_gen[x] = [y]

        if x in self.next_gen.keys() and y in self.next_gen[x]:
            return True
        else:
            return False

    def get_next_generation(self, x_min, y_min, x_max, y_max):

        for x in range(x_min, x_max):
            for y in range(y_min, y_max):
                self.next_generation(x, y)

        self.pos = self.next_gen
        self.next_gen = {}


class DeadZoneField(Field):
    def __init__(self, x, y):
        super().__init__()
        self.x = x
        self.y = y

    def set(self, x, y, value):
        if((x >= 0 and x < self.x) and (y >= 0 and y < self.y)):
            super().set(x, y, value)

    def count_neighbors_alive(self, x, y):
        neighbors = get_neighbors(x, y)

        for width in neighbors.keys():
            for height in neighbors[width]:
                if ((width < 0 or width >= self.x)
                        and (height < 0 or height >= self.y)):

                    neighbors[width].remove(height)

        counter = super().count_neighbors_alive(neighbors)

        return counter

    def next_generation(self, x, y):

        neighbors_alive = self.count_neighbors_alive(x, y)
        next_gen = super().next_generation(x, y, neighbors_alive)

        return next_gen

    def get_next_generation(self):

        super().get_next_generation(0, 0, self.x, self.y)


class WrappingField(Field):
    def __init__(self, x, y):
        super().__init__()
        self.x = x
        self.y = y

    def set(self, x, y, value):
        x = x % self.x
        y = y % self.y

        super().set(x, y, value)

    def count_neighbors_alive(self, x, y):
        neighbors = {
            (x-1) % self.x: [(y-1) % self.y, y % self.y, (y+1) % self.y],
            x % self.x: [(y-1) % self.y, (y+1) % self.y],
            (x+1) % self.x: [(y-1) % self.y, y % self.y, (y+1) % self.y]
        }
        counter = super().count_neighbors_alive(neighbors)

        return counter

    def next_generation(self, x, y):

        neighbors_alive = self.count_neighbors_alive(x, y)
        next_gen = super().next_generation(x, y, neighbors_alive)

        return next_gen

    def get_next_generation(self):

        super().get_next_generation(0, 0, self.x, self.y)


class EndlessField(Field):
    def __init__(self):
        super().__init__()

    def count_neighbors_alive(self, x, y):
        neighbors = get_neighbors(x, y)

        counter = super().count_neighbors_alive(neighbors)

        return counter

    def next_generation(self, x, y):

        neighbors_alive = self.count_neighbors_alive(x, y)
        next_gen = super().next_generation(x, y, neighbors_alive)

        return next_gen

    def get_size_of_field(self):

        x_min = min(self.pos.keys())
        y_min = self.pos[x_min][0]
        x_max = max(self.pos.keys())
        y_max = self.pos[x_max][0]

        for x_element in range(x_min, x_max):
            for y_element in self.pos[x_element]:
                if y_element < y_min:
                    y_min = y_element
                if y_element > y_max:
                    y_max = y_element

        x_min -= 1
        y_min -= 1
        x_max += 2
        y_max += 2

        return x_min, y_min, x_max, y_max

    def get_next_generation(self):

        x_min, y_min, x_max, y_max = self.get_size_of_field()
        super().get_next_generation(x_min, y_min, x_max, y_max)
